package com.justcode.samplewebapp.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.justcode.samplewebapp.model.DAO;
import com.justcode.samplewebapp.model.Contact;


public class TestCaseDataTest {
	

	@Test
	public void create() {
		
		for (int i = 0; i < 5; i++) {
			Contact contact = new Contact("Name " + i, "Address " + i);
			DAO.createContact(contact);
			assertTrue("Contact Creation Problem",contact.getId()==i+1);
		}
	
	}		

	@Test
	public void list() {
		
		List<Contact> list = DAO.getContacts();
		assertTrue("Contact List Null",list!=null);
		assertTrue("Create Failed",list.size() == 6);
		
	}


}
