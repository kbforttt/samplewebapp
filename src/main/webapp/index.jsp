<%@ page import="com.justcode.samplewebapp.model.DAO,com.justcode.samplewebapp.model.Contact,java.util.List" %>
<%
	String name = request.getParameter("name");
	String email = request.getParameter("email");
	if(name!=null && email!=null){
		Contact contact = new Contact(name,email);
		DAO.createContact(contact);
	}
%>

<html>
<head>
<title>Sample Web Application</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>

<form method=post>
Name
<input name=name type=text>
Email
<input name=email type=text>
<input type=submit>
</form>

<table>
<tr><th>ID</th><th>Name</th><th>Email</th></tr>
<%
	List<Contact> list = DAO.getContacts();
	for(Contact contact:list){
		String tr = String.format("<tr><td>%s</td><td>%s</td><td>%s</td></tr>", contact.getId(), contact.getName(), contact.getEmail());
		out.write(tr);
	}
%>
</table>

</body>
</html>
