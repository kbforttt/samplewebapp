package com.justcode.samplewebapp.model;

import java.util.List;


import java.util.Properties;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.*;

public class DAO {

	private static SessionFactory sessionFactory = null;  
	
	static{
	    Configuration configuration = new Configuration();  
	    configuration.configure();  
	    
	    Properties properties = configuration.getProperties();
	    
	    ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(properties).buildServiceRegistry();          
	    sessionFactory = configuration.buildSessionFactory(serviceRegistry);  
	}
	
	
	public static void createContact(Contact contact){
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(contact);
		session.flush();
		tx.commit();
		session.close();
		
	}
	
	public static List<Contact> getContacts() {

		Session session = sessionFactory.openSession();
		@SuppressWarnings("unchecked")
		List<Contact> contactList = session.createQuery("from Contact").list();
		session.close();
		return contactList;
	}
	
	

	

}
